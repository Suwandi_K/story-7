from django.db import models

# Create your models here.
class Warna(models.Model):
    kode_warna = models.CharField(max_length = 16, unique=True)

class Pesan(models.Model):
    nama = models.CharField(max_length=32)
    pesan = models.TextField()
    tanggal_dibuat = models.DateTimeField(auto_now=True)
    warna = models.ForeignKey(Warna, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.nama + ' - ' + self.pesan