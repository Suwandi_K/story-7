from django.shortcuts import render, redirect
from .models import Pesan , Warna

# Create your views here.
def index(request, sign = ''):
    if request.method == 'POST':
        if request.POST['isi'] == '':
            sign = 'failed'
        else:
            Pesan.objects.create(
            nama = request.POST.get('nama'),
            pesan = request.POST.get('isi')
            )
            sign = 'success'
    status = Pesan.objects.all()
    return render(request, 'index.html',{'status' : status , 'sign' : sign})

def konfirmasi_pesan(request):
    if request.method == 'POST':
        if request.POST['nama'] != '':
            nama = request.POST['nama']
        else :
            nama = 'Anonymous'
        context = {
            'nama':nama,
            'pesan':request.POST['isi'],
        }
        return render(request,'konfirmasipesan.html', context)
    return redirect('/')

def ubah_warna(request, postid):
    try:
        pesan = Pesan.objects.get(id=postid)
        try: 
            id_warna_sekarang = pesan.warna.id
        except:
            id_warna_sekarang = 0
        
        id_warna_terakhir = Warna.objects.latest('id').id
        if id_warna_sekarang+1 > id_warna_terakhir:
            warna = Warna.objects.all().first()
        else:
            warna = Warna.objects.filter(id__gt=id_warna_sekarang).order_by('id').first()
        pesan.warna = warna
        pesan.save()
    except Pesan.DoesNotExist:
        pass
    return redirect('/')
