from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Pesan)
admin.site.register(models.Warna)