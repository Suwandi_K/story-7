from django.test import TestCase , Client , override_settings
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .models import Pesan , Warna
from .views import index , konfirmasi_pesan
import time

# Create your tests here.
@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class Story7UnitTest(TestCase):
    def test_create_models(self):
        pesan = Pesan.objects.create(
            nama='Toni',
            pesan='Test'
        )
        self.assertEqual(str(pesan), 'Toni - Test')

    def test_checking_homepage(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_not_exist(self):
        response = self.client.get("/ngulang_ppw/")
        self.assertEqual(response.status_code, 404)
    
    def test_get_confirmed_data_without_post_method(self):
        response = self.client.get("/konfirmasipesan/")
        self.assertEqual(response.status_code, 302)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func,index)

    def test_check_home_page_have_form_and_title(self):
        request = HttpRequest()
        response = index(request)
        isi_html = response.content.decode('utf8')
        self.assertIn("Pesan", isi_html)
        self.assertIn('<form', isi_html)

    def test_add_status(self):
        Pesan.objects.create(
            nama='Nalton', 
            pesan='123'
        )
        self.assertEqual(Pesan.objects.all().count(), 1)

    def test_confirm_a_status(self):
        name = 'Denny'
        status = 'hai'
        hasil = self.client.post('/konfirmasipesan/', data={
            'nama': name,
            'isi': status,
        })
        self.assertContains(hasil, name)
        self.assertContains(hasil, status)

    def test_failed_sending_form(self):
        response = self.client.post('/konfirmasipesan/', data={
            'nama': '',
            'isi': '',
        })
        jumlah = Pesan.objects.all().count()
        self.assertEqual(jumlah,0)
        self.assertEqual(response.status_code, 200)

    def test_no_object_saved_of_post_data_in_index(self):
        hasil = self.client.post('/', data={
            'nama': '',
            'isi': '',
        })
        jumlah = Pesan.objects.all().count()
        self.assertEqual(jumlah,0)   

    def test_exist_object_of_post_data_in_index(self):
        hasil = self.client.post('/', data={
            'nama': 'hai',
            'isi': 'hai',
        })
        jumlah = Pesan.objects.all().count()
        self.assertEqual(jumlah,1)   

    def test_check_not_error_when_changing_color_non_exist_post(self):
        response = self.client.get('/ubahwarna/1/')
        self.assertEqual(response.status_code, 302)

    def test_check_changing_color_back_to_begining(self):
        name = 'Humas'
        status = 'Halo'
        Pesan.objects.create(
            nama=name, 
            pesan=status,
        )
        test_color = ['#D1C4E9', '#B2EBF2']
        for color in test_color:
            Warna.objects.create(kode_warna = color)

        data = Pesan.objects.get(nama=name)
        for x in range(len(test_color)+1):
            self.client.get('/ubahwarna/'+str(data.id)+'/')

        data = Pesan.objects.get(nama=name)
        
        self.assertEqual(data.warna.kode_warna, test_color[0])

class Story7FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Story7FunctionalTest, self).tearDown()


    def test_input_form_and_confirm_data(self):
        # Opening the link we want to test
        self.browser.get('http://127.0.0.1:8000')

        # find the form element
        nama = self.browser.find_element_by_id('nama')
        status = self.browser.find_element_by_id('isi')

        submit = self.browser.find_element_by_id('submit')

        # Fill the form with data
        nama.send_keys('Tommy')
        status.send_keys('halo')

        # submitting the form
        submit.click()

        # Checking confirm page showing name and status submitted
        self.assertIn('Tommy', self.browser.page_source)
        self.assertIn('halo', self.browser.page_source)
       